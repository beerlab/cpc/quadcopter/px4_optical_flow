ARG BASE_IMG

FROM ${BASE_IMG}

SHELL ["/bin/bash", "-ci"]
ENV DEBIAN_FRONTEND noninteractive

WORKDIR /workspace/ros_ws

RUN apt-get update && apt-get install -y \
    ros-noetic-mavros ros-noetic-mavros-extras ros-noetic-mavros-msgs \
    ros-noetic-usb-cam ros-noetic-teleop-twist-keyboard ros-noetic-tf && \
    rm -rf /var/lib/apt/lists/*

RUN sh -c "$(wget https://raw.githubusercontent.com/mavlink/mavros/master/mavros/scripts/install_geographiclib_datasets.sh -O -)"

COPY ./optical_flow_to_twist src/optical_flow_to_twist
COPY ./mavros_optical_plugin src/mavros_optical_plugin
RUN catkin build