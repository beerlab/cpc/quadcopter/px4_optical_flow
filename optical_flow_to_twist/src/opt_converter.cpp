#include "ros/ros.h"
#include "geometry_msgs/TwistStamped.h"
#include "geometry_msgs/Twist.h"

#include <tf/transform_listener.h>
#include <string>
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <mavros_optical_plugin/OpticalFlow.h>
#include <queue>

#define DISTANCE_BUFFER_SIZE 1
#define VELOCITY_BUFFER_SIZE 1
#define MIN_DT 0.001
#define COEFF 0.1

ros::Time lastTime;
ros::Publisher velPub;
ros::Subscriber optSub;

int distBuffSize = DISTANCE_BUFFER_SIZE;
int velBuffSize = VELOCITY_BUFFER_SIZE;

std::queue<double> distanceBuffer;
std::queue<double> zVelocityBuffer;
double distanceBufferSum;
double zVelocityBufferSum;

double lastDistanceValue;
double currDistanceValue;
double zVelocityValue;

geometry_msgs::Twist velMsg;
geometry_msgs::TwistStamped velStampedMsg;

void converterCallback(const mavros_optical_plugin::OpticalFlow::ConstPtr& msg)
{

    velMsg.linear.x = COEFF * msg->flow.x;
    velMsg.linear.y = COEFF * msg->flow.y;

    distanceBuffer.push(msg->ground_distance);
    distanceBufferSum += distanceBuffer.back();
    if (distanceBuffer.size() > distBuffSize)
    {
        distanceBufferSum -= distanceBuffer.front();
        distanceBuffer.pop();
    }
    currDistanceValue = distanceBufferSum / distanceBuffer.size();
    
    zVelocityValue = (currDistanceValue - lastDistanceValue) / std::max(MIN_DT, (ros::Time::now()-lastTime).toSec());
    zVelocityBuffer.push(zVelocityValue);
    zVelocityBufferSum += zVelocityBuffer.back();
    if (zVelocityBuffer.size() > velBuffSize)
    {
        zVelocityBufferSum -= zVelocityBuffer.front();
        zVelocityBuffer.pop();
    }
    velMsg.linear.z = zVelocityBufferSum / zVelocityBuffer.size();

    lastDistanceValue = currDistanceValue;
    lastTime = ros::Time::now();

    velStampedMsg.twist = velMsg;
    velStampedMsg.header.stamp = lastTime;
    velPub.publish(velStampedMsg);
}

int main(int argc, char **argv)
{
    distanceBufferSum = 0.;
    zVelocityBufferSum = 0.;
    lastDistanceValue = 0.;
    currDistanceValue = 0.;
    zVelocityValue = 0.;

    std::string vel_topic;

    ros::init(argc, argv, "optical_flow_to_twist");
    ros::NodeHandle n("~");
    
    if (n.hasParam("velocity_topic"))
    {
        n.getParam("velocity_topic", vel_topic);
        velPub = n.advertise<geometry_msgs::TwistStamped>(vel_topic, 1);
    } else {
        velPub = n.advertise<geometry_msgs::TwistStamped>("/optical_vel", 1);
    }

    if (n.hasParam("distance_buff_size"))
    {
        n.getParam("distance_buff_size", distBuffSize);
    } 

    if (n.hasParam("velocity_buff_size"))
    {
        n.getParam("velocity_buff_size", velBuffSize);
    } 

    optSub = n.subscribe("/mavros/optical_flow/raw/optical_flow", 10, converterCallback);
    lastTime = ros::Time::now();

    ros::spin();
    return 0;
}

