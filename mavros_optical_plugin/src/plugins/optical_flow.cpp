/**
 * @brief PX4Flow plugin
 * @file px4flow.cpp
 * @author M.H.Kabir <mhkabir98@gmail.com>
 * @author Vladimir Ermakov <vooon341@gmail.com>
 *
 * @addtogroup plugin
 * @{
 */
/*
 * Copyright 2014 M.H.Kabir.
 * Copyright 2016 Vladimir Ermakov
 *
 * This file is part of the mavros package and subject to the license terms
 * in the top-level LICENSE file of the mavros repository.
 * https://github.com/mavlink/mavros/tree/master/LICENSE.md
 */

#include <mavros/mavros_plugin.h>

#include <mavros_optical_plugin/OpticalFlow.h>
#include <sensor_msgs/Temperature.h>
#include <sensor_msgs/Range.h>
#include <geometry_msgs/Vector3.h>

namespace mavros {
namespace extra_plugins {
/**
 * @brief PX4 Optical Flow plugin
 *
 * This plugin can publish data from PX4Flow camera to ROS
 */
class OpticalFlowPlugin : public plugin::PluginBase {
public:
	OpticalFlowPlugin() : PluginBase(),
		flow_nh("~optical_flow"),
		ranger_fov(0.0),
		ranger_min_range(0.3),
		ranger_max_range(5.0)
	{ }

	void initialize(UAS &uas_) override
	{
		PluginBase::initialize(uas_);

		flow_nh.param<std::string>("frame_id", frame_id, "optical_flow");

		/**
		 * @note Default rangefinder is Maxbotix HRLV-EZ4
		 * This is a narrow beam (60cm wide at 5 meters,
		 * but also at 1 meter). 6.8 degrees at 5 meters, 31 degrees
		 * at 1 meter
		 */
		flow_nh.param("ranger_fov", ranger_fov, 0.119428926);

		flow_nh.param("ranger_min_range", ranger_min_range, 0.3);
		flow_nh.param("ranger_max_range", ranger_max_range, 5.0);

		flow_pub = flow_nh.advertise<mavros_optical_plugin::OpticalFlow>("raw/optical_flow", 10);
		range_pub = flow_nh.advertise<sensor_msgs::Range>("ground_distance", 10);

	}

	Subscriptions get_subscriptions() override
	{
		return {
			make_handler(&OpticalFlowPlugin::handle_optical_flow)
		};
	}

private:
	ros::NodeHandle flow_nh;

	std::string frame_id;

	double ranger_fov;
	double ranger_min_range;
	double ranger_max_range;

	ros::Publisher flow_pub;
	ros::Publisher range_pub;

	void handle_optical_flow(const mavlink::mavlink_message_t *msg, mavlink::common::msg::OPTICAL_FLOW &flow)
	{
		auto header = m_uas->synchronized_header(frame_id, flow.time_usec);

		/**
		 * Raw message with axes mapped to ROS conventions and temp in degrees celsius.
		 *
		 * The optical flow camera is essentially an angular sensor, so conversion is like
		 * gyroscope. (aircraft -> baselink)
		 */
		auto flow_xy = ftf::transform_frame_aircraft_baselink(
			Eigen::Vector3d(
			flow.flow_x,
			flow.flow_y,
			0.0));

		auto flow_comp_xy = ftf::transform_frame_aircraft_baselink(
			Eigen::Vector3d(
			flow.flow_comp_m_x,
			flow.flow_comp_m_y,
			0.0));
		auto flow_msg = boost::make_shared<mavros_optical_plugin::OpticalFlow>();
		geometry_msgs::Vector3 flow_vec;
		flow_vec.x = flow_xy[0];
		flow_vec.y = flow_xy[1];

		geometry_msgs::Vector3 flow_comp_vec;
		flow_comp_vec.x = flow.flow_rate_x;
		flow_comp_vec.y = flow.flow_rate_y;

		geometry_msgs::Vector3 flow_rate_vec;
		flow_rate_vec.x = flow_comp_xy[0];
		flow_rate_vec.y = flow_comp_xy[1];

		flow_msg->header = header;
		flow_msg->flow = flow_vec;
		flow_msg->flow_comp_m = flow_comp_vec;
		flow_msg->ground_distance = flow.ground_distance;
		flow_msg->quality = flow.quality;
		flow_msg->flow_rate = flow_rate_vec;

		flow_pub.publish(flow_msg);

		// Rangefinder
		/**
		 * @todo: use distance_sensor plugin only to publish this data
		 * (which receives DISTANCE_SENSOR msg with multiple rangefinder
		 * sensors data)
		 *
		 * @todo: suggest modification on MAVLink OPTICAL_flow msg
		 * which removes sonar data fields from it
		 */
		auto range_msg = boost::make_shared<sensor_msgs::Range>();

		range_msg->header = header;

		range_msg->radiation_type = sensor_msgs::Range::ULTRASOUND;
		range_msg->field_of_view = ranger_fov;
		range_msg->min_range = ranger_min_range;
		range_msg->max_range = ranger_max_range;
		range_msg->range = flow.ground_distance;

		range_pub.publish(range_msg);
	}
};

}	// namespace extra_plugins
}	// namespace mavros

#include <pluginlib/class_list_macros.hpp>
PLUGINLIB_EXPORT_CLASS(mavros::extra_plugins::OpticalFlowPlugin, mavros::plugin::PluginBase)
